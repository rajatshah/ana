Refer to the Source tab to see the contents of the folders within Bitbucket.

Each folder contains the Visual Studio projects used to implement the experiments for comparing the performance of ANA* with other anytime variants of A*.

ANAStar_2D_gridworld_5000x5000 contains the C++ project for the 4-way gridworld environment of size 5000x5000.  This experiment can be run with random or uniform edge cost.

ANAStar_Multiple_Sequence_Alignment contains the C++ source files for the experiment that simulates the alignment of 6 molecular protein sequences.

ANAStar_SBPL_Library contain Maxim Likhachev's SBPL Library including the ANA* algorithm as one of the planners.  The code can be configured to run the Robot Arm experiment in 6-D and 20-D, as well as the 8-way gridworld environment of size 1200x100 with uniform cost edges.