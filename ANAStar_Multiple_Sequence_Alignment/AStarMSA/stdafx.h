// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <cmath>
#include <algorithm>
#include <stdio.h>
#include <tchar.h>
#include <limits.h>
#include <map>
#include <queue>
#include <vector>
#include <iostream>
#include "binaryHeap.h"
#include <time.h>
#include <string>


#define NUM_SEQUENCES 5

const std::string proteins[NUM_SEQUENCES] = { std::string ("KKQCGVLEGLKVKSEWGRAYGSGHDREAFSQAIWRATFAQVPESRSLFKRVHGDDTSHPAFIAHAERVLGGLDIAISTLDQPATLKEELDHLQVQHEGRKIPDNYFDAFKTAILHVVAAQLGRCYDREAWDACIDHIEDGIKGHH"),
                                        std::string ("MSLSAAEADLAGKSWAPVFANKDANGDAFLVALFEKFPDSANFFADFKGKSVADIKASPKLRDVSSRIFTRLNEFVNNAADAGKMSAMLSQFAKEHVGFGVGSAQFENVRSMFPGFVASVAAPPAGADAAWTKLFGLIIDALKAAGK"),
                                        std::string ("GLDGAQKTALKESWKVLGADGPTMMKNGSLLFGLLFKTYPDTKKHFKHFDDATFAAMDTTGVGKAHGVAVFSGLGSMICSIDDDDCVBGLAKKLSRNHLARGVSAADFKLLEAVFKZFLDEATQRKATDAQKDADGALLTMLIKAHV"),
                                        std::string ("MKFFAVLALCIVGAIASPLSADQAALVKSTWAQVRNSEVEILAAVFTAYPDIQARFPQFAGKDVASIKDTGAFATHAGRIVGFVSEIIALIGNESNAPAVQTLVGQLAASHKARGISQAQFNEFRAGLVSYVSSNVAWNAAAESAWTAGLDNIFGLLFAAL"),
                                        std::string ("VATPAMPSMTDAQVAAVKGDWEKIKGSGVEILYFFLNKFPGNFPMFKKLGNDLAAAKGTAEFKDQADKIIAFLQGVIEKLGSDMGGAKALLNQLGTSHKAMGITKDQFDQFRQALTELLGNLGFGGNIGAWNATVDLMFHVIFNALDGTPV") };


// proteins[0] = Extracellular globin of Lumbricus terrestris - AIII
// proteins[1] = Myoglobin of Aplysia limacina
//	proteins[2] = Dimeric myoglobin of Busycon canaliculatum
//	proteins[3] = Monomeric hemoglobin of Chironomus thummithummi - VIIA 
//	proteins[4] = Monomeric insect hemoglobin of Chironomus thummi thummi � IIIa


#define INFTY 1000000000
            //2147483647
#define PI 3.1415926535897932384626433832795

typedef std::vector<bool> bit_vector;

struct Coord {
  int val[5];
  
  Coord() { }
  Coord(const Coord& right) {
    val[0] = right.val[0];
    val[1] = right.val[1];
		val[2] = right.val[2];
    val[3] = right.val[3];
		val[4] = right.val[4];
  }
  Coord(int _x0, int _x1, int _x2, int _x3, int _x4) {
    
		val[0]=_x0;
		val[1]=_x1;
		val[2]=_x2;
		val[3]=_x3;
		val[4]=_x4;
  }

  inline bool operator<(const Coord& right) const {
    for (int i = 0; i < 5; ++i) {
			if (val[i] < right.val[i]) {
				return true;
			} else if (val[i] > right.val[i]) {
				return false;
			}
		}
		return false;
  }

  inline bool operator==(const Coord& right) const {
    return (val[0] == right.val[0] && val[1] == right.val[1] && val[2] == right.val[2] && val[3] == right.val[3] && val[4] == right.val[4]);
  }

  inline bool operator!=(const Coord& right) const {
    return (val[0] != right.val[0] || val[1] != right.val[1] || val[2] != right.val[2] || val[3] != right.val[3] || val[4] != right.val[4]);
  }

};

struct Cell {
  Cell() {
    traversable = true;
    g = INFTY;
    h = 0;
    pos_in_open = 0;
    pos_in_incons = 0;
    closed = -1;
  }

  bool traversable;
  int g;
  int h;
  int closed;
  std::map<Coord, Cell>::iterator bp;
  binaryHeap<std::pair<double, int>, std::map<Coord, Cell>::iterator>::pointer pos_in_open;
  binaryHeap<std::pair<double, int>, std::map<Coord, Cell>::iterator>::pointer pos_in_incons;
};



// Heap functions




// TODO: reference additional headers your program requires here
