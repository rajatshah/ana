#include <vector>
#include <utility>

template <class Key, class Type> 
class binaryHeap {
public:
  struct Contents {
    size_t pos;
    Key key;
    Type val;

    inline Contents(const Key& _key, const Type& _val, size_t _pos) {
      key = _key;
      val = _val;
      pos = _pos;
    }
  };

  typedef Contents* pointer;

private:
  std::vector<pointer> heap;

  inline size_t left(size_t n) {
    return (n+1)*2-1;
  }

  inline size_t right(size_t n) {
    return (n+1)*2;
  }

  inline size_t parent(size_t n) {
    return (n+1)/2-1;
  }

  inline void increase(size_t elt) {
    while (left(elt) < heap.size() && 
          (heap[left(elt)]->key < heap[elt]->key || (right(elt) < heap.size() && heap[right(elt)]->key < heap[elt]->key))) {
      if (right(elt) < heap.size() && heap[right(elt)]->key < heap[left(elt)]->key) { // swap right
        std::swap(heap[right(elt)]->pos, heap[elt]->pos);
        std::swap(heap[right(elt)], heap[elt]);
        elt = right(elt);
      } else { // swap left
        std::swap(heap[left(elt)]->pos, heap[elt]->pos);
        std::swap(heap[left(elt)], heap[elt]);
        elt = left(elt);
      }
    }
  }

  inline void decrease(size_t elt) {
    while (elt != 0 && heap[elt]->key < heap[parent(elt)]->key) {
      std::swap(heap[parent(elt)]->pos, heap[elt]->pos);
      std::swap(heap[parent(elt)], heap[elt]);
      elt = parent(elt);
    }
  }

public:

  inline binaryHeap() { }

  inline ~binaryHeap() {
    for (size_t elt = 0; elt < heap.size(); ++elt) {
      delete heap[elt];
    }
  }

  inline pointer insert(const Key& key, const Type& val) {
    pointer p = new Contents(key, val, heap.size());
    heap.push_back(p);
    
    decrease(heap.size() - 1);
    return p;
  }

  inline pointer insert_unsafe(const Key& key, const Type& val) {
    pointer p = new Contents(key, val, heap.size());
    heap.push_back(p);
    return p;
  }

  inline void erase(pointer p) {
    size_t elt = p->pos;
    if (elt == heap.size() - 1) {
      delete heap[elt];
      heap.pop_back();
    } else {
      delete heap[elt];
      heap[elt] = heap.back();
      heap.pop_back();
      heap[elt]->pos = elt;
      if (elt != 0 && heap[elt]->key < heap[parent(elt)]->key) {
        decrease(elt);
      } else {
        increase(elt);
      }
    }
  }

  inline bool empty() const {
    return (heap.empty());
  }

  inline const Type& front() const {
    return heap[0]->val;
  }
  inline Type& front() {
    return heap[0]->val;
  }

  inline const Type& operator[](size_t elt) const {
    return heap[elt]->val;
  }
  inline Type& operator[](size_t elt) {
    return heap[elt]->val;
  }

  inline void pop() {
    erase(heap[0]);
  }

  inline void heapify() {
    for (size_t i = parent(heap.size() - 1); i != -1; --i) {
      increase(i);
    }
  }

  inline void decreaseKey(pointer p, const Key& key) {
    p->key = key;
    decrease(p->pos);
  }

  inline void changeKey_unsafe(size_t elt, const Key& key) {
    heap[elt]->key = key;
  }

  inline void erase_unsafe(size_t elt) {
    delete heap[elt];
    if (elt != heap.size() - 1) {
      heap[elt] = heap.back();
      heap[elt]->pos = elt;
    }
    heap.pop_back();
  }

  inline size_t size() const {
    return heap.size();
  }

  inline void clear() {
    for (size_t elt = 0; elt < heap.size(); ++elt) {
      delete heap[elt];
    }
    heap.clear();
  }

  inline const Key& key(size_t elt) const {
    return heap[elt]->key;
  }
};