// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <cmath>
#include <algorithm>
#include <stdio.h>
#include <tchar.h>
#include <limits.h>
#include <map>
#include <queue>
#include <vector>
#include <iostream>
#include "binaryHeap.h"
#include <time.h>

#define INFTY 1000000000
            //2147483647
#define PI 3.1415926535897932384626433832795

typedef std::vector<bool> bit_vector;

struct Coord {
  int val[2];
  
  Coord() { }
  Coord(const Coord& right) {
    val[0] = right.val[0];
    val[1] = right.val[1];
  }
  Coord(int _x, int _y) {
    val[0] = _x;
    val[1] = _y;
  }

  inline bool operator<(const Coord& right) const {
    return (val[0] < right.val[0] || (val[0] == right.val[0] && val[1] < right.val[1]));
  }

  inline bool operator==(const Coord& right) const {
    return (val[0] == right.val[0] && val[1] == right.val[1]);
  }

  inline bool operator!=(const Coord& right) const {
    return (val[0] != right.val[0] || val[1] != right.val[1]);
  }
};

struct Cell {
  Cell() {
    traversable = true;
    g = INFTY;
    h = 0;
    pos_in_open = 0;
    pos_in_incons = 0;
    closed = -1;
  }

  bool traversable;
  int g;
  int h;
  int closed;
  std::map<Coord, Cell>::iterator bp;
  binaryHeap<std::pair<double, int>, std::map<Coord, Cell>::iterator>::pointer pos_in_open;
  binaryHeap<std::pair<double, int>, std::map<Coord, Cell>::iterator>::pointer pos_in_incons;
};



// Heap functions




// TODO: reference additional headers your program requires here
