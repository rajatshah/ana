// AStar.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#define WORKSPACE 5000


#define COST_BOUND 0
#define TIME_BOUND 100000

std::map<Coord, Cell> env;
binaryHeap<std::pair<double, int>, std::map<Coord, Cell>::iterator> open, incons;

std::vector<int> w_space(WORKSPACE*WORKSPACE);
Coord wGoal;
Coord basePos;

Coord start;
std::map<Coord, Cell>::iterator goal_it;

int G; 
int open_op;
int count;
double eps;
int iterationNumber;

clock_t startTime;

/// BEGIN Environment Specific Functions

inline int round(double d)
{
  return (int) floor(d + 0.5);
}

inline int collisionCheck(const Coord& n) {
  return w_space[n.val[1]*WORKSPACE + n.val[0]];
}

inline void initw_space() {
	for(int y=0; y<WORKSPACE; ++y) {
		for(int x=0; x<WORKSPACE; ++x) {
			//w_space[(y*WORKSPACE) + x] = (1*WORKSPACE)-abs(x-y);
			
			if((rand()%100)<35 && (y!=0 && x!=0) && (y!=WORKSPACE-1 && x!=WORKSPACE-1)) {
				w_space[(y*WORKSPACE) + x] = -1;
			}
			else {
				w_space[(y*WORKSPACE) + x] = (rand() % 1000) + 1;
			}
			/*
			w_space[(y*WORKSPACE) + x] = (rand() % 1000) + 1;
			*/
		}
		
	}
}

inline void init() {
  // BEGIN Environment Specific
	//std::cout << "Suboptimality: " << epsprime << std::endl;
	//std::cout << WORKSPACE << " x " << WORKSPACE << " gridworld, random cost." << std::endl;
  initw_space();
  
  start.val[0] = 0; start.val[1] = 0;
  wGoal.val[0] = WORKSPACE-1; wGoal.val[1] = WORKSPACE-1;

  // END Environment Specific

  goal_it = env.end();
  G = INFTY;
  open_op = 0;
  count = 0;
  iterationNumber = 0;
}


inline void createNeighbors(const Coord& s, std::vector<std::pair<Coord, int> >& neighbors) {
  for (int i = -1; i <= 1; ++i) {
    for (int j = -1; j <= 1; ++j) {
      Coord n(s.val[0] + i, s.val[1] + j);
      if (i*j == 0 && n.val[0] >= 0 && n.val[1] >= 0 && n.val[0] < WORKSPACE && n.val[1] < WORKSPACE && n != s) {
			int x =	collisionCheck(n);
			if(x>=0) {
				neighbors.push_back(std::make_pair(n, x));
			}
      }
    }
  }
}

inline int h(const Coord& s) {
  int x_diff = abs(wGoal.val[0] - s.val[0]);
  int y_diff = abs(wGoal.val[1] - s.val[1]);
	return y_diff + x_diff;
}

inline bool isGoal(const Coord& s) {
  return (s == wGoal); 
}

/// END Environment Specific Functions

inline double e(std::map<Coord, Cell>::iterator s_it) {
  if (s_it->second.h == 0) {
    if (s_it->second.g >= G) {
      return 0;
    } else {
      return INFTY;
    }
  } else {
    return (G - s_it->second.g) / (double) s_it->second.h;
  }
}

inline double f(std::map<Coord, Cell>::iterator s_it) {
	return s_it->second.g + eps * s_it->second.h;
}

inline void publishSolution(double epsprime) {
	std::cout << "Suboptimality: " << epsprime << "\tTime: " << (clock() - startTime) / (double) CLOCKS_PER_SEC << "\tCost: " << G << " Count: " << count << std::endl;



 /* std::cout << "Suboptimality: " << epsprime << std::endl;
  std::cout << "Time: " << (clock() - startTime) / (double) CLOCKS_PER_SEC << std::endl;
  std::cout << "Cost: " << G << std::endl;
  std::cout << "Open: " << open.size() << std::endl;
  std::cout << "Count: " << count << std::endl;
  std::cout << "Count2: " << -open_op << std::endl << std::endl;*/
  
  /*std::map<Coord, Cell>::iterator s_it = goal_it;
  while (s_it != env.end()) {
    std::cout << s_it->first << std::endl;
    s_it = s_it->second.bp;
  }
  std::cout << std::endl;*/
}

inline bool improvePathEKey() {
  while (!open.empty()) {
	  if(((clock() - startTime) / (double) CLOCKS_PER_SEC) > TIME_BOUND) {
		  std::cout << "Epsilon: " << eps << " Time: " << (clock() - startTime) / (double) CLOCKS_PER_SEC << std::endl;
		  exit(1);
	  }
    if (-open.key(0).first < eps) {
      eps = -open.key(0).first;
	}
    std::map<Coord, Cell>::iterator s_it = open.front();
    open.pop();  // remove state with largest e-value from OPEN
    s_it->second.pos_in_open = 0;
		
    if (isGoal(s_it->first)) {
      G = s_it->second.g;
      goal_it = s_it;
      return true;
    }

    ++count; // count of expanded states

    std::vector<std::pair<Coord, int> > neighbors;
    createNeighbors(s_it->first, neighbors);

    for (size_t i = 0; i < neighbors.size(); ++i) {
      const Coord& n = neighbors[i].first;
      const int& c = neighbors[i].second;  // c is the cost to go from one cell to another. The cells are always adjacent
      
      std::map<Coord, Cell>::iterator n_it = env.find(n);
      if (n_it == env.end()) {
        Cell cell;
        //cell.traversable = collisionCheck(n);
        cell.h = h(n);
        n_it = env.insert(std::make_pair(n,cell)).first;
      }

      if (n_it->second.traversable && s_it->second.g + c + n_it->second.h < G && s_it->second.g + c < n_it->second.g) { 
        // put or update n in open
        n_it->second.g = s_it->second.g + c;
        n_it->second.bp = s_it;

        if (n_it->second.pos_in_open != 0) { // already in OPEN, update f-value
          open.decreaseKey(n_it->second.pos_in_open, std::make_pair(-e(n_it), --open_op));
        } else { // not in OPEN, insert
          n_it->second.pos_in_open = open.insert(std::make_pair(-e(n_it), --open_op), n_it);
        }
		}
		}
	} 
  return false;
}

inline void AStarEKey() {
  eps = INFTY;
  
  Cell cell;
  cell.g = 0;
  cell.h = h(start);
  cell.bp = env.end();
    
  std::map<Coord, Cell>::iterator start_it = env.insert(std::make_pair(start, cell)).first;
  start_it->second.pos_in_open = open.insert(std::make_pair(-e(start_it), --open_op), start_it);
  
	while(!open.empty() && G > COST_BOUND) {
    bool result = improvePathEKey();
    
    // better path found
    // update e-values (prune states with e-value <= 1) and reheapify 
    // compute epsilon bound of path

    double epsprime = 1;

    for (size_t i = 0; i < open.size(); ) {
      if (G / (double) (open[i]->second.g + open[i]->second.h) > epsprime) {
        epsprime = G / (double) (open[i]->second.g + open[i]->second.h);
      }

      if (e(open[i]) <= 1) {
        open[i]->second.pos_in_open = 0;
        open.erase_unsafe(i);
      } else {
        open.changeKey_unsafe(i, std::make_pair(-e(open[i]), open.key(i).second));
        ++i;
      }
    }
    open.heapify();

    // publish solution
    if (result) {
		publishSolution(epsprime);
    }
	}
  std::cout << "Solution Cost: "<< G << " Time: " << (clock() - startTime) / (double) CLOCKS_PER_SEC << std::endl;
}

inline bool improvePathInconsOpt() {
  ++iterationNumber;

  while (!open.empty()) { 
    std::map<Coord, Cell>::iterator s_it = open.front();
    open.pop();  // remove state with largest e-value from OPEN
    s_it->second.pos_in_open = 0;
		
    if (isGoal(s_it->first)) {
      G = s_it->second.g;
      goal_it = s_it;
      return true;
    }

    ++count; // count of expanded states

    //place that state s onto the closed list
		s_it->second.closed = iterationNumber;
    
    std::vector<std::pair<Coord, int> > neighbors;
    createNeighbors(s_it->first, neighbors);

    for (size_t i = 0; i < neighbors.size(); ++i) {
      const Coord& n = neighbors[i].first;
      const int& c = neighbors[i].second;  // c is the cost to go from one cell to another. The cells are always adjacent
     
      std::map<Coord, Cell>::iterator n_it = env.find(n);
      if (n_it == env.end()) {
        Cell cell;
        //cell.traversable = collisionCheck(n);
        cell.h = h(n);
        n_it = env.insert(std::make_pair(n,cell)).first;
      }

      if (n_it->second.traversable && s_it->second.g + c + n_it->second.h < G && s_it->second.g + c < n_it->second.g) { 
        // put or update n in open or incons
        n_it->second.g = s_it->second.g + c;
        n_it->second.bp = s_it;

        if (f(n_it) >= G) { // || n_it->second.closed == iterationNumber) { // Closed or not good enough for this iteration
          // Push on incons
          if (n_it->second.pos_in_incons != 0) { // already in INCONS, update e-value
            incons.decreaseKey(n_it->second.pos_in_incons, std::make_pair(-e(n_it), --open_op));
          } else {
            n_it->second.pos_in_incons = incons.insert(std::make_pair(-e(n_it), --open_op), n_it);
          }
				} else { // Not closed and good enough for this iteration
          // Push in in Open list
          if (n_it->second.pos_in_incons != 0) { // currently in INCONS, move to OPEN
            incons.erase(n_it->second.pos_in_incons);
            n_it->second.pos_in_incons = 0;
            n_it->second.pos_in_open = open.insert(std::make_pair(f(n_it), --open_op), n_it);
          } else if (n_it->second.pos_in_open != 0) { // already in OPEN, update f-value
            open.decreaseKey(n_it->second.pos_in_open, std::make_pair(f(n_it), --open_op));
          } else { // in INCONS nor OPEN
            n_it->second.pos_in_open = open.insert(std::make_pair(f(n_it), --open_op), n_it);
          }
				}
	    }
		}
	} 
  return false;
}

inline void AStarInconsOpt() {
  eps = 10000;
  
  Cell cell;
  cell.g = 0;
  cell.h = h(start);
  cell.bp = env.end();

  std::map<Coord, Cell>::iterator start_it = env.insert(std::make_pair(start, cell)).first;
  start_it->second.pos_in_open = open.insert(std::make_pair(f(start_it), open_op), start_it);
  
  while(!open.empty()) {
    if (improvePathInconsOpt()) { // better path found
      // move states from open into incons and resort incons with new e-value (prune states with e-value <= 1)
      double epsprime = 1;
      for (size_t i = 0; i < incons.size(); ) {
        if (G / (double) (incons[i]->second.g + incons[i]->second.h) > epsprime) {
          epsprime = G / (double) (incons[i]->second.g + incons[i]->second.h);
        }

        if (e(incons[i]) <= 1) {
          incons[i]->second.pos_in_incons = 0;
          incons.erase_unsafe(i);
        } else {
          incons.changeKey_unsafe(i, std::make_pair(-e(incons[i]), incons.key(i).second));
          ++i;
        }
      }
      for (size_t i = 0; i < open.size(); ++i) {
        if (G / (double) (open[i]->second.g + open[i]->second.h) > epsprime) {
          epsprime = G / (double) (open[i]->second.g + open[i]->second.h);
        }

        open[i]->second.pos_in_open = 0;
        if (e(open[i]) > 1) {
          open[i]->second.pos_in_incons = incons.insert_unsafe(std::make_pair(-e(open[i]), open.key(i).second), open[i]);
        }
      }
      open.clear();
      incons.heapify();

      printf_s("%24.24g \n", eps);
      publishSolution(epsprime);
    }
    	  
    // move first state of incons onto open and set epsilon
    if (!incons.empty()) {
      if (-incons.key(0).first < eps) {
        eps = -incons.key(0).first;
        //std::cout << "Epsilon: " << eps << " Time: " << (clock() - startTime) / (double) CLOCKS_PER_SEC << std::endl;
      }
      while (!incons.empty() && -incons.key(0).first >= eps) {
        incons.front()->second.pos_in_open = open.insert(std::make_pair(f(incons.front()), incons.key(0).second), incons.front());
        incons.front()->second.pos_in_incons = 0;
        incons.pop();
      }
    }
	}
  std::cout << "Epsilon: 1  Time: " << (clock() - startTime) / (double) CLOCKS_PER_SEC << std::endl;
}

inline bool improvePathARAStar() {
  ++iterationNumber;

  while (!open.empty() && open.key(0).first < G) {
	   if(((clock() - startTime) / (double) CLOCKS_PER_SEC) > 1000) {
		  std::cout << "Epsilon: " << eps << " Time: " << (clock() - startTime) / (double) CLOCKS_PER_SEC << std::endl;
		  exit(1);
	  }
    std::map<Coord, Cell>::iterator s_it = open.front();
    open.pop();  // remove state with largest f-value from OPEN
    s_it->second.pos_in_open = 0;
		
    if (isGoal(s_it->first)) {
      G = s_it->second.g;
      goal_it = s_it;
      return true;
    }

    ++count; // count of expanded states

    // place that state s onto the closed list
		s_it->second.closed = iterationNumber;
    
    std::vector<std::pair<Coord, int> > neighbors;
    createNeighbors(s_it->first, neighbors);

    for (size_t i = 0; i < neighbors.size(); ++i) {
      const Coord& n = neighbors[i].first;
      const int& c = neighbors[i].second;  // c is the cost to go from one cell to another. The cells are always adjacent
     
      std::map<Coord, Cell>::iterator n_it = env.find(n);
      if (n_it == env.end()) {
        Cell cell;
        //cell.traversable = collisionCheck(n);
        cell.h = h(n);
        n_it = env.insert(std::make_pair(n,cell)).first;
      }

      if (n_it->second.traversable && s_it->second.g + c + n_it->second.h < G && s_it->second.g + c < n_it->second.g) { 
      
        // put or update n in open or incons
        n_it->second.g = s_it->second.g + c;
        n_it->second.bp = s_it;

        if (n_it->second.closed == iterationNumber) { // Closed
          // Push on incons
          if (n_it->second.pos_in_incons != 0) { // already in INCONS, update f-value
            incons.changeKey_unsafe(n_it->second.pos_in_incons->pos, std::make_pair(0, --open_op));
          } else {
            n_it->second.pos_in_incons = incons.insert_unsafe(std::make_pair(0, --open_op), n_it);
          }
				} else { // Not closed
          // Push in in Open list
          if (n_it->second.pos_in_open != 0) { // already in OPEN, update f-value
            open.decreaseKey(n_it->second.pos_in_open, std::make_pair(f(n_it), --open_op));
          } else { // not in OPEN
            n_it->second.pos_in_open = open.insert(std::make_pair(f(n_it), --open_op), n_it);
          }
				}
	    }
		}
	} 
  return false;
}

inline void ARAStar(double initeps, double decreps) {
  eps = initeps;
  
  Cell cell;
  cell.g = 0;
  cell.h = h(start);
  cell.bp = env.end();

  std::map<Coord, Cell>::iterator start_it = env.insert(std::make_pair(start, cell)).first;
  start_it->second.pos_in_open = open.insert(std::make_pair(f(start_it), open_op), start_it);
  
  while(!open.empty()) {
    bool result = improvePathARAStar();

    //std::cout << "Epsilon: " << eps << " Time: " << (clock() - startTime) / (double) CLOCKS_PER_SEC << std::endl;
    
    eps -= decreps;
    
    // move states from incons into open and resort open with new f-value (prune states with e-value <= 1)
    double epsprime = 1;
    for (size_t i = 0; i < open.size(); ) {
      if (G / (double) (open[i]->second.g + open[i]->second.h) > epsprime) {
        epsprime = G / (double) (open[i]->second.g + open[i]->second.h);
      }
      if (e(open[i]) <= 1) {
        open[i]->second.pos_in_open = 0;
        open.erase_unsafe(i);
      } else {
        open.changeKey_unsafe(i, std::make_pair(f(open[i]), open.key(i).second));
        ++i;
      }
    }
    for (size_t i = 0; i < incons.size(); ++i) {
      if (G / (double) (incons[i]->second.g + incons[i]->second.h) > epsprime) {
        epsprime = G / (double) (incons[i]->second.g + incons[i]->second.h);
      }
      if (e(incons[i]) > 1) {
        incons[i]->second.pos_in_open = open.insert_unsafe(std::make_pair(f(incons[i]), incons.key(i).second), incons[i]);
      }
      incons[i]->second.pos_in_incons = 0;
    }
    incons.clear();
    open.heapify();

    if (result) { // better path found
      
		publishSolution(eps);
    }
	}
  std::cout << "Epsilon: 1  Time: " << (clock() - startTime) / (double) CLOCKS_PER_SEC << std::endl;
}


inline bool improvePathAnytimeAStar() {
  while (!open.empty()) { // && G >= open.key(0).first) {
    std::map<Coord, Cell>::iterator s_it = open.front();
    open.pop();  // remove state with largest e-value from OPEN
    s_it->second.pos_in_open = 0;
		
    if (isGoal(s_it->first)) {
      G = s_it->second.g;
      goal_it = s_it;
      return true;
    }

    ++count; // count of expanded states

    std::vector<std::pair<Coord, int> > neighbors;
    createNeighbors(s_it->first, neighbors);

    for (size_t i = 0; i < neighbors.size(); ++i) {
      const Coord& n = neighbors[i].first;
      const int& c = neighbors[i].second;  // c is the cost to go from one cell to another. The cells are always adjacent
     
      std::map<Coord, Cell>::iterator n_it = env.find(n);
      if (n_it == env.end()) {
        Cell cell;
        //cell.traversable = collisionCheck(n);
        cell.h = h(n);
        n_it = env.insert(std::make_pair(n,cell)).first;
      }

      if (n_it->second.traversable && s_it->second.g + c + n_it->second.h < G && s_it->second.g + c < n_it->second.g) { 
        // put or update n in open
        n_it->second.g = s_it->second.g + c;
        n_it->second.bp = s_it;

        // Push in in Open list
        if (n_it->second.pos_in_open != 0) { // already in OPEN, update f-value
          open.decreaseKey(n_it->second.pos_in_open, std::make_pair(f(n_it), --open_op));
        } else { // not in OPEN
          n_it->second.pos_in_open = open.insert(std::make_pair(f(n_it), --open_op), n_it);
        }
				
	    }
		}
	} 
  return false;
}

inline void AnytimeAStar(double initeps) {
  eps = initeps;
 
  Cell cell;
  cell.g = 0;
  cell.h = h(start);
  cell.bp = env.end();

  std::map<Coord, Cell>::iterator start_it = env.insert(std::make_pair(start, cell)).first;
  start_it->second.pos_in_open = open.insert(std::make_pair(f(start_it), open_op), start_it);
  
  while(!open.empty()) {
    bool result = improvePathAnytimeAStar();

    // prune states from open with e-value <= 1
    double epsprime = 1;
    for (size_t i = 0; i < open.size(); ) {
      if (G / (double) (open[i]->second.g + open[i]->second.h) > epsprime) {
        epsprime = G / (double) (open[i]->second.g + open[i]->second.h);
      }
      if (e(open[i]) <= 1) {
        open[i]->second.pos_in_open = 0;
        open.erase_unsafe(i);
      } else {
        ++i;
      }
    }
    open.heapify();

    if (result) {
      publishSolution(epsprime);
    }
	}
  std::cout << "Epsilon: 1  Time: " << (clock() - startTime) / (double) CLOCKS_PER_SEC << std::endl;
}


inline bool improvePathRWAStar() {
  ++iterationNumber;

  while (!open.empty()) { // && open.key(0).first < G) {
    std::map<Coord, Cell>::iterator s_it = open.front();
    open.pop();  // remove state with largest e-value from OPEN
    s_it->second.pos_in_open = 0;
		
    if (isGoal(s_it->first)) {
      G = s_it->second.g;
      goal_it = s_it;
      return true;
    }

    ++count; // count of expanded states

    std::vector<std::pair<Coord, int> > neighbors;
    createNeighbors(s_it->first, neighbors);

    for (size_t i = 0; i < neighbors.size(); ++i) {
      const Coord& n = neighbors[i].first;
      const int& c = neighbors[i].second;  // c is the cost to go from one cell to another. The cells are always adjacent
     
      std::map<Coord, Cell>::iterator n_it = env.find(n);
      if (n_it == env.end()) {
        Cell cell;
        //cell.traversable = collisionCheck(n);
        cell.h = h(n);
        cell.closed = INFTY;
        n_it = env.insert(std::make_pair(n,cell)).first;
      }

      if (n_it->second.traversable && s_it->second.g + c + n_it->second.h < G && (s_it->second.g + c < n_it->second.g || n_it->second.closed < iterationNumber)) { 
        // put or update n in open
        if (s_it->second.g + c < n_it->second.g) {
          n_it->second.g = s_it->second.g + c;
          n_it->second.bp = s_it;
        }
        n_it->second.closed = iterationNumber; // closed takes role of ``seen''
               
        // Push in in Open list
        if (n_it->second.pos_in_open != 0) { // already in OPEN, update f-value
          open.decreaseKey(n_it->second.pos_in_open, std::make_pair(f(n_it), --open_op));
        } else { // not in OPEN
          n_it->second.pos_in_open = open.insert(std::make_pair(f(n_it), --open_op), n_it);
        }
				
	    }
		}
	} 
  return false;
}

inline void RWAStar(double initeps, double decreps) {
  eps = initeps;

  Cell cell;
  cell.g = 0;
  cell.h = h(start);
  cell.bp = env.end();

  std::map<Coord, Cell>::iterator start_it = env.insert(std::make_pair(start, cell)).first;
 
  while(eps >= 1) {
    start_it->second.closed = iterationNumber + 1;
    start_it->second.pos_in_open = open.insert(std::make_pair(f(start_it), open_op), start_it);

    bool result = improvePathRWAStar();

    std::cout << "Epsilon: " << eps << "  Time: " << (clock() - startTime) / (double) CLOCKS_PER_SEC << std::endl;

    double epsprime = 1;
    for (size_t i = 0; i < open.size(); ++i) {
      if (G / (double) (open[i]->second.g + open[i]->second.h) > epsprime) {
        epsprime = G / (double) (open[i]->second.g + open[i]->second.h);
      }
      open[i]->second.pos_in_open = 0;
    }

    if (result) {
      publishSolution(epsprime);
    }

    eps -= decreps;

    open.clear();
	}
  std::cout << "Epsilon: 1  Time: " << (clock() - startTime) / (double) CLOCKS_PER_SEC << std::endl;
}


int _tmain(int argc, _TCHAR* argv[])
{
  init();

  startTime = clock();
  std::cout << "Running A* with cost bound " << COST_BOUND << std::endl;
  //AStarEKey();
  //AStarInconsOpt();
  ARAStar(400, 0.2);
  //AnytimeAStar(10);
  //RWAStar(2, 0.2);


  return 0;
}
  